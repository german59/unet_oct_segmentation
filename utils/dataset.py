import os
import cv2
import numpy as np
import albumentations as T
import settings as cfg
import matplotlib.pyplot as plt

from PIL import Image
from utils.utils import visualize, create_dir
from torch.utils.data import DataLoader, Dataset
from albumentations.pytorch import ToTensorV2
from utils.open_volFiles import generate_I_GT, split_data


def create_data():
    if not os.path.exists(cfg.IMAGES_PATH) or not os.path.exists(cfg.MASK_PATH):
        create_dir(cfg.IMAGES_PATH)
        create_dir(cfg.MASK_PATH)
        print("Reading OCT files")
        generate_I_GT()
    if not os.path.exists(cfg.TRAIN_IMAGES_CROP) or not os.path.exists(cfg.TRAIN_MASKS_CROP):
        print("Creating Train-Val directories")
        split_data(train_size=cfg.TRAIN_SIZE)
    train_transforms = T.Compose(
        [
            # T.Resize(height=cfg.IMAGE_SIZE, width=cfg.IMAGE_SIZE),
            T.Rotate(limit=(-cfg.DEGREES, cfg.DEGREES), p=1.0),
            T.HorizontalFlip(p=0.5),
            T.CLAHE(clip_limit=2.0, tile_grid_size=(3, 3), p=0.5),
            # T.Affine(scale=(0.9, 1.1), p=0.5),
            # T.Normalize(mean=[0.0, 0.0, 0.0], std=[255., 255.0, 255.0]),
            ToTensorV2(),
        ]
    )
    val_transforms = T.Compose(
        [
            # T.Resize(height=cfg.IMAGE_SIZE, width=cfg.IMAGE_SIZE),
            # T.Normalize(mean=[0.2991, 0.2991, 0.2991], std=[0.1757, 0.1757, 0.1757]),
            ToTensorV2(),
        ]
    )
    train_loader, val_loader = get_loaders(
        train_dir=cfg.TRAIN_IMAGES_CROP,
        train_maskdir=cfg.TRAIN_MASKS_CROP,
        val_dir=cfg.VAL_IMAGES_CROP,
        val_maskdir=cfg.VAL_MASKS_CROP,
        batch_size=cfg.BATCH_SIZE,
        train_transform=train_transforms,
        val_transform=val_transforms,
        num_workers=os.cpu_count(),
        pin_memory=True,
        channels=cfg.CHANNELS
    )
    return train_loader, val_loader


def get_loaders(train_dir,
                train_maskdir,
                val_dir,
                val_maskdir,
                batch_size,
                train_transform,
                val_transform,
                num_workers=os.cpu_count(),
                pin_memory=True,
                channels=3
                ):
    train_ds = OctDataset(image_dir=train_dir,
                          mask_dir=train_maskdir,
                          transform=train_transform,
                          channels=channels
                          )
    val_ds = OctDataset(image_dir=val_dir,
                        mask_dir=val_maskdir,
                        transform=val_transform,
                        channels=channels
                        )
    train_loader = DataLoader(
        train_ds,
        batch_size=batch_size,
        num_workers=num_workers,
        pin_memory=pin_memory,
        shuffle=True,
    )
    val_loader = DataLoader(
        val_ds,
        batch_size=batch_size,
        num_workers=num_workers,
        pin_memory=pin_memory,
        shuffle=False,
    )
    return train_loader, val_loader


class OctDataset(Dataset):
    def __init__(self, image_dir, mask_dir, transform=None, channels=3):
        self.image_dir = image_dir
        self.mask_dir = mask_dir
        self.transform = transform
        self.images = os.listdir(image_dir)
        self.channels = channels

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):
        img_path = os.path.join(self.image_dir, self.images[index])
        mask_path = os.path.join(self.mask_dir, self.images[index])
        if self.channels == 3:
            image = np.array(Image.open(img_path).convert('RGB')) # / 255.
        else:
            image = np.array(Image.open(img_path)) # / 255.

        mask = np.array(Image.open(mask_path))

        if self.transform is not None:
            augmentations = self.transform(image=image, mask=mask)
            image = augmentations["image"]
            mask = augmentations["mask"]
        return image, mask


def test():
    train_transforms = T.Compose(
        [
            T.Rotate(limit=(-15, 15), p=1.0), #border_mode=cv2.BORDER_CONSTANT
            T.HorizontalFlip(p=0.5),
            T.Affine(scale=(0.9, 1.1), p=0.5),
            T.CLAHE(clip_limit=2.0, tile_grid_size=(3, 3), p=1.0)
            # T.Normalize(mean=[-0.2991, -0.2991, -0.2991], std=[0.1757, 0.1757, 0.1757]),
        ]
    )
    train_ds = OctDataset(image_dir=cfg.TRAIN_IMAGES_CROP,
                          mask_dir=cfg.TRAIN_MASKS_CROP,
                          transform=train_transforms,
                          )
    randint = np.random.randint(low=0, high=len(train_ds))
    for i in range(3):
        image, mask = train_ds[randint]
        print(image)
        visualize(image=image, mask=mask.squeeze())
    plt.show()


if __name__ == "__main__":
    test()  # test
