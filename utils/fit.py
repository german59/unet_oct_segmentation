import sys
import torch
import numpy as np
from tqdm import tqdm
from torch.utils.tensorboard import SummaryWriter
import math
import settings as cfg
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F


def tensorboard_checkpoint(step, writer, train_loss, val_loss, train_iou, val_iou):
    results_loss = {'Train': train_loss, 'Validation': val_loss}
    results_iou = {'Train': train_iou, 'Validation': val_iou}
    writer.add_scalars("Loss", results_loss, step)
    writer.add_scalars("IoU", results_iou, step)


def fit(num_epochs,
        train_loader,
        val_loader,
        model,
        optimizer,
        loss_fn,
        metric,
        device,
        checkpoint_path,
        base_lr,
        scheduler
        ):

    best_valid_loss = float("inf")

    """ Create log interface """
    writer = SummaryWriter()
    iter_num = 0.0
    iter_val = 0.0
    # images, _ = next(iter(train_loader))
    # writer.add_graph(model, images.to(device).float())

    for epoch in range(num_epochs):
        print(f"Epoch: {epoch+1}/{num_epochs}")

        train_loss, train_iou, iter_num = train_fn(
            loader=train_loader,
            model=model,
            writer=writer,
            optimizer=optimizer,
            loss_fn=loss_fn,
            device=device,
            metric=metric,
            base_lr=base_lr,
            iter_num=iter_num,
            max_epochs=num_epochs,
            scheduler=scheduler
        )
        scheduler.step()
        val_loss, val_iou, iter_val = validation(model, val_loader, loss_fn, metric, device, iter_val, writer)

        tensorboard_checkpoint(epoch, writer, train_loss, val_loss, train_iou, val_iou)

        """ Saving the model """
        if val_loss < best_valid_loss:
            str_print = f"Valid loss improved from {best_valid_loss:2.4f} to {val_loss:2.4f}. Saving checkpoint: {checkpoint_path}"
            best_valid_loss = val_loss
            torch.save(model, checkpoint_path + '/model.pth')
            torch.save(model.state_dict(), checkpoint_path + "/weights.pth")
        else:
            str_print = f"Valid loss not improved: {best_valid_loss:2.4f}"

        print(f'--> Train IoU: {train_iou:.4f} \t Val. IoU: {val_iou:.4f}')
        print(f'--> Train Loss: {train_loss:.4f} \t Val. Loss: {val_loss:.4f}')
        print(str_print)
    writer.close()


def train_fn(loader, model, writer, optimizer, loss_fn, device, metric, base_lr, iter_num, max_epochs, scheduler):
    train_loss = 0.0
    train_iou = 0.0
    loop = tqdm(loader, ascii=True, ncols=150)
    max_iterations = max_epochs * len(loader)
    model.train()

    for batch_idx, (x, y) in enumerate(loop):
        x = x.type(torch.float).to(device)
        y = y.type(torch.long).to(device)
        # print()
        # print(torch.max(x), torch.max(y),)
        # sys.exit()
        # forward
        y_pred = model(x)
        loss = loss_fn(y_pred, y)
        # backward
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        # lr scheduler
        # lr_ = base_lr * (math.exp(-iter_num * cfg.FACTOR_LR / max_iterations))

        lr_ = optimizer.param_groups[0]["lr"]
        # optimizer.param_groups[0]["lr"] = base_lr * (math.exp(-iter_num * cfg.FACTOR_LR / max_iterations))
        iter_num = iter_num + 1
        # metrics
        m0 = metric(y_pred, y)
        # accumulate metrics and loss items
        train_iou += m0.mean()
        train_loss += loss.item()
        # update tqdm loop
        if cfg.CLASSES == 4:
            loop.set_postfix(OPL=m0[1], EZ=m0[2], ELM=m0[3], loss=loss.item(), lr=lr_)
        if cfg.CLASSES == 3:
            loop.set_postfix(OPL=m0[1], EZ=m0[2], loss=loss.item(), lr=lr_)
        # save scalars in tensorboard
        writer.add_scalar("Train/Loss", loss.item(), iter_num)
        writer.add_scalar("Train/mIoU", m0.mean(), iter_num)
        writer.add_scalar("lr", lr_, iter_num)

    return train_loss/len(loader), train_iou/len(loader), iter_num


def validation(model, loader, loss_fn, metric, device, iter_val, writer):
    valid_loss = 0.0
    valid_iou = 0.0
    loop = tqdm(loader, ascii=True, ncols=150)
    model.eval()
    with torch.no_grad():
        for batch_idx, (x, y) in enumerate(loop):
            x = x.type(torch.float).to(device)
            y = y.type(torch.long).to(device)
            y_pred = model(x)
            loss = loss_fn(y_pred, y)
            valid_loss += loss.item()
            m0 = metric(y_pred, y)
            valid_iou += m0.mean()
            # save scalars in tensorboard
            writer.add_scalar("Validation/Loss", loss.item(), iter_val)
            writer.add_scalar("Validation/mIoU", m0.mean(), iter_val)
            if cfg.CLASSES == 4:
                loop.set_postfix(OPL=m0[1], EZ=m0[2], ELM=m0[3], loss=loss.item())
            if cfg.CLASSES == 3:
                loop.set_postfix(OPL=m0[1], EZ=m0[2], loss=loss.item())
            iter_val += 1
            # tensorboard images
            if iter_val % 50 == 0:
                gt = image_tensorboard(y[:3, :, :].unsqueeze(1), device)
                pred = F.softmax(y_pred[:3, :, :, :], dim=1)
                pred = torch.argmax(pred, dim=1).unsqueeze(1)
                pred = image_tensorboard(pred, device)
                writer.add_images(f'Data', x[:3, :, :, :], iter_val)
                writer.add_images(f'Ground truth', gt, iter_val)
                writer.add_images(f'Prediction', pred, iter_val)
    return valid_loss/len(loader), valid_iou/len(loader), iter_val


def image_tensorboard(img, device):
    img_rgb = torch.zeros((img.size(0), 3, img.size(2), img.size(3))).float().to(device)
    # print(np.max(img))
    # sys.exit()
    img_rgb[:, :, :, :] = torch.where(img == 3, 1, 0)
    img_rgb[:, 0, :, :] = torch.where(img.squeeze(1) == 2, 1, 0)
    img_rgb[:, 1, :, :] = torch.where(img.squeeze(1) == 1, 1, 0)
    img_rgb[:, 2, :, :] = torch.where(img.squeeze(1) == 0, 1, 0)
    return img_rgb
