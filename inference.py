import sys
import torch
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from patchify import patchify
import settings as cfg
from utils.unet import Unet
from torchsummary import summary
from utils.unet_transformer import SwinUnet
import torch.nn.functional as F
import os
import multiprocessing as mp

def get_filenames(path, ext):
    X0 = []
    for i in sorted(os.listdir(path)):
        if i.endswith(ext):
            X0.append(os.path.join(path, i))
    return X0


def plot_inference():
    image_path = cfg.VAL_IMAGES
    mask_path = cfg.VAL_MASKS
    files_images = get_filenames(cfg.VAL_IMAGES, 'bmp')
    files_masks = get_filenames(cfg.VAL_MASKS, 'bmp')
    argv = sys.argv[1:]
    np.random.seed(int(argv[1]))  # 2, 10, 20, 30 seed randome
    name = argv[0]
    x = np.random.randint(0, len(files_images))
    pick_image = files_images[x]

    large_image = np.array(Image.open(pick_image))
    patches_images = patchify(np.array(large_image), (224, 224), step=224)

    pick_mask = files_masks[x]
    large_mask = np.array(Image.open(pick_mask).convert('L'))
    # print(large_mask.shape)
    # sys.exit()
    patches_mask = patchify(np.array(large_mask), (224, 224), step=224)

    pred = np.zeros(patches_images.shape)

    for i in range(patches_images.shape[0]):
        for j in range(patches_images.shape[1]):
            single_image = patches_images[i, j, :, :]
            pred[i, j, :, :] = prediction(single_image, name)
    img1 = patches_images[0, :, :, :]
    img2 = patches_images[1, :, :, :]
    imgh1 = np.hstack((img1))
    imgh2 = np.hstack((img2))
    img = np.vstack((imgh1, imgh2))
    msk1 = patches_mask[0, :, :, :]
    msk2 = patches_mask[1, :, :, :]
    mskh1 = np.hstack((msk1))
    mskh2 = np.hstack((msk2))
    msk = np.vstack((mskh1, mskh2))
    pred1 = pred[0, :, :, :]
    pred2 = pred[1, :, :, :]
    predh1 = np.hstack((pred1))
    predh2 = np.hstack((pred2))
    predict = np.vstack((predh1, predh2))
    # mask = np.add(zeros, mask)
    figure, ax = plt.subplots(nrows=3, ncols=1)
    figure.set_figwidth(12)
    figure.set_figheight(12)
    ax[0].imshow(img, cmap='gray')
    ax[0].title.set_text('Test image')
    ax[1].imshow(msk, cmap='jet')
    ax[1].title.set_text('Ground Truth')
    ax[2].imshow(predict, cmap='jet')
    ax[2].title.set_text('Prediction')
    figure.suptitle(name, fontsize=24)
    plt.savefig('dataset/save_patches1/' + os.path.splitext(os.path.split(pick_image)[1])[0] + '_' + name + '.png')
    plt.show()


def inference_path(image):
    argv = sys.argv[1:]
    name = argv[0]

    large_image = np.array(Image.open(image))
    patches_images = patchify(np.array(large_image), (224, 224), step=224)
    pred = np.zeros(patches_images.shape)

    for i in range(patches_images.shape[0]):
        for j in range(patches_images.shape[1]):
            single_image = patches_images[i, j, :, :]
            pred[i, j, :, :] = prediction(single_image, name)
    img1 = patches_images[0, :, :, :]
    img2 = patches_images[1, :, :, :]
    imgh1 = np.hstack((img1))
    imgh2 = np.hstack((img2))
    img = np.vstack((imgh1, imgh2))
    img = Image.fromarray(img)
    img.save('dataset/preds/Images/' + os.path.splitext(os.path.split(image)[1])[0] + '_' +name + '.png')
    pred1 = pred[0, :, :, :]
    pred2 = pred[1, :, :, :]
    predh1 = np.hstack((pred1))
    predh2 = np.hstack((pred2))
    predict = np.vstack((predh1, predh2)) * 255.
    predict = predict.astype('uint8')
    # plt.figure()
    # plt.imshow(predict)
    # plt.show()
    predict = Image.fromarray(predict).convert('L')
    predict.save('dataset/preds/Masks/' + os.path.splitext(os.path.split(image)[1])[0] + '_' + name + '.png')
    # sys.exit()


def prediction(image, name):
    """ CUDA device """
    device = torch.device("cuda")
    if name == 'unet':
        PATH = 'files/weights.pth'
        # best_model = Unet(
        #     num_classes=cfg.CLASSES,
        #     input_channels=1,
        #     num_layers=cfg.LAYERS,
        #     features_start=cfg.FEATURES,
        #     bilinear=False,
        #     dp=cfg.DP,
        #     kernel_size=(cfg.KERNEL_SIZE, cfg.KERNEL_SIZE),
        #     padding=cfg.PADDING,
        #     stride=cfg.STRIDE
        # ).to(device)
        # image1 = image / 255.
        # image1 = torch.Tensor(image1).unsqueeze(0).unsqueeze(0).float().to(device)
    elif name == 'swin':
        PATH = 'checkpoints/06APR/06_22-21_SWIN_OCT/weights.pth'
        image1 = np.array(Image.fromarray(image).convert('RGB')) / 255.
        image1 = np.transpose(image1, (2, 0, 1))
        image1 = torch.Tensor(image1).unsqueeze(0).float().to(device)
        best_model = SwinUnet(in_chans=cfg.CHANNELS, img_size=cfg.IMAGE_SIZE, num_classes=cfg.CLASSES)
        best_model = best_model.to(device)
    else:
        print('Invalid model name!')
        sys.exit()
    load = torch.load(PATH)
    best_model.load_state_dict(load)
    pr_mask = best_model(image1)
    pr_mask = F.softmax(pr_mask, dim=1)
    pr_mask = torch.argmax(pr_mask, dim=1)
    single_mask = (pr_mask.squeeze().cpu().long().detach().numpy()) / 3
    return single_mask


def main():
    files = get_filenames(cfg.VAL_IMAGES, 'bmp')
    # plot_inference()
    for file in files:
        print(file)
        inference_path(file)




if __name__ == '__main__':
    # plot_inference()
    main()