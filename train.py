import datetime
import numpy as np
from utils.fit import fit
from utils.dataset import create_data
import settings as cfg
from matplotlib import pyplot as plt
from utils.utils import create_dir, seeding
from utils.loss import *
from utils.metrics import IoU
from torch.optim.lr_scheduler import StepLR, ExponentialLR
from utils.unet_transformer import SwinUnet
from utils.unet import Unet
import sys
from torchsummary import summary


def main():
    """ Seeding """
    # seeding(42) #42

    """ Configuration parameters """
    lr = cfg.LEARNING_RATE
    batch_size = cfg.BATCH_SIZE
    num_epochs = cfg.EPOCHS
    model_name = cfg.MODEL_NAME
    if model_name == 'unet':
        checkpoint_path = "checkpoints/" + \
            datetime.datetime.now().strftime("%d_%H-%M_UNET_OCT/")
    elif model_name == 'swin':
        checkpoint_path = "checkpoints/" + \
            datetime.datetime.now().strftime("%d_%H-%M_SWIN_OCT/")
    else:
        print('Invalid model!')
        sys.exit()
    gpus_ids = cfg.GPUS_ID

    """ CUDA device """
    device = torch.device(f"cuda")

    """ Dataset and loader """
    train_loader, val_loader = create_data()

    """ Building model """
    if model_name == 'unet':
        model = Unet(
            num_classes=cfg.CLASSES,
            input_channels=cfg.CHANNELS,
            num_layers=cfg.LAYERS,
            features_start=cfg.FEATURES,
            bilinear=cfg.BILINEAR,
            dp=cfg.DP,
            kernel_size=(cfg.KERNEL_SIZE, cfg.KERNEL_SIZE),
            padding=cfg.PADDING,
            stride=cfg.STRIDE
        ).to(device)
    elif model_name == 'swin':
        model = SwinUnet(in_chans=cfg.CHANNELS, img_size=cfg.IMAGE_SIZE,
                         num_classes=cfg.CLASSES).to(device)
        model.state_dict()
        model.load_from("files/swin_tiny_patch4_window7_224.pth", device)
    else:
        print('Invalid name model!')
        sys.exit()
    summary(model, input_size=(cfg.CHANNELS,
            cfg.IMAGE_SIZE, cfg.IMAGE_SIZE), batch_size=-1)
    if len(gpus_ids) > 1:
        model = nn.DataParallel(model, device_ids=gpus_ids)
    pytorch_total_params = sum(p.numel() for p in model.parameters())

    """ Prepare training """
    if cfg.CLASSES == 3:
        class_weights = [1, 1, 1]
    elif cfg.CLASSES == 4:
        class_weights = [1, 1, 1, 1]
    else:
        print("Invalid classes!!")
        sys.exit()
    optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay=cfg.WEIGHT_DECAY,
                                 betas=(cfg.BETA1, cfg.BETA2))
    loss_fn = WeightedCrossEntropyDice(
        class_weights=class_weights, device=device)
    loss_fn = WeightedCrossEntropyDice(
        class_weights=class_weights, device=device)
    metrics = IoU(device)
    scheduler = StepLR(optimizer, step_size=cfg.STEP_SIZE, gamma=cfg.GAMMA)
    
    """ Directories """
    create_dir("checkpoints")
    create_dir(checkpoint_path)

    """ Save params LOG file """
    with open(checkpoint_path + "LOGS.txt", "w") as text_file:
        text_file.write(f"Learning rate: {lr}\n")
        text_file.write(f"weight_decay: {cfg.WEIGHT_DECAY}\n")
        text_file.write(f"BETA1, BETA2: {cfg.BETA1, cfg.BETA2}\n")
        text_file.write(f"Optimizer: {cfg.OPTIMIZER}\n")
        text_file.write(f"Epochs: {num_epochs}\n")
        text_file.write(f"Loss function: {loss_fn.__name__}\n")
        text_file.write(f"Batch size: {batch_size}\n")
        text_file.write(f"Factor LR{cfg.FACTOR_LR}\n")
        text_file.write(f"Metric: {metrics.__name__}\n")
        text_file.write(f"Num classes: {cfg.CLASSES}\n")
        text_file.write(f"No. of Parameters: {pytorch_total_params}\n")
        text_file.write(f"No. of GPUs: {len(gpus_ids)}\n")
        text_file.close()

    """ Training the model """
    fit(num_epochs=num_epochs,
        train_loader=train_loader,
        val_loader=val_loader,
        model=model,
        optimizer=optimizer,
        loss_fn=loss_fn,
        metric=metrics,
        device=device,
        checkpoint_path=checkpoint_path,
        base_lr=lr,
        scheduler=scheduler
        )


if __name__ == '__main__':
    main()
