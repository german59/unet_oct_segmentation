import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import settings as cfg
from inference import get_filenames
import sys

image_path = cfg.VAL_IMAGES
mask_path = cfg.VAL_MASKS
files_images = get_filenames(image_path, 'bmp')
files_masks = get_filenames(mask_path, 'bmp')
x = np.random.randint(0, len(files_images))
pick_image = files_images[x]
image = np.array(Image.open(pick_image))
pick_mask = files_masks[x]
mask = np.array(Image.open(pick_mask))

figure, ax = plt.subplots(nrows=2, ncols=1)
figure.set_figwidth(12)
figure.set_figheight(12)
ax[0].imshow(image, cmap='gray')
ax[0].title.set_text('Test image')
ax[1].imshow(mask, cmap='jet')
ax[1].title.set_text('Ground Truth')
plt.show()

# checkpoint_path = 'checkpoints/06_11-17_SWIN_OCT/'
# """ Saving numpy history """
# hist_train_loss = np.load(checkpoint_path + 'hist_train_loss.npy')
# hist_val_loss = np.load(checkpoint_path + 'hist_val_loss.npy')
# hist_train_IoU = np.load(checkpoint_path + 'hist_train_IoU.npy')
# hist_val_IoU = np.load(checkpoint_path + 'hist_val_IoU.npy')
# x = np.arange(1, len(hist_train_loss) + 1)
#
# with open(checkpoint_path + "results.txt", "w") as ff:
#     ff.write(f"Best Train Loss: {np.min(hist_train_loss):0.4f}, Best Val Loss:{np.min(hist_val_loss):0.4f},\n "
#              f"Best Train IoU:{np.max(hist_train_IoU):0.4f}, Best Val IoU:{np.max(hist_val_IoU):0.4f}\n")
#
# plt.figure()
# plt.plot(x, hist_train_loss)
# plt.plot(x, hist_val_loss)
# plt.grid(color='lightgray', linestyle='-', linewidth=2)
# plt.ylim(0.08,0.2)
# plt.ylabel('Loss')
# plt.xlabel('Epoch')
# plt.legend([f'Train min:{np.min(hist_train_loss):0.4f}', f'Val min:{np.min(hist_val_loss):0.4f}'], loc='best')
# plt.savefig(checkpoint_path + "loss.png")
#
# plt.figure()
# plt.plot(x, hist_train_IoU)
# plt.plot(x, hist_val_IoU)
# plt.grid(color='lightgray', linestyle='-', linewidth=2)
# plt.ylim(0.6,0.82)
# plt.ylabel('Loss')
# plt.xlabel('Epoch')
# plt.legend([f'Train max:{np.max(hist_train_IoU):0.4f}', f'Val max:{np.max(hist_val_IoU):0.4f}'], loc='best')
# plt.savefig(checkpoint_path + "IoU.png")
#
# base_lr = 0.05
# len_loader = 23
# max_epochs = 800
# max_iterations = max_epochs * len_loader
# iter_num = np.arange(0, max_iterations)
# lr_ = base_lr * (1.0 - iter_num / max_iterations) ** 0.9
# plt.figure(figsize=(12, 8))
# plt.title('Schedule Learning Rate', fontsize=24)
# plt.plot(iter_num, lr_)
# plt.grid('on')
# plt.xlabel('Iterations', fontsize=16)
# plt.ylabel('Learning rate (log)', fontsize=16)
# plt.yscale('log')
# plt.savefig('schadule_learning_rate.png')
# plt.show()