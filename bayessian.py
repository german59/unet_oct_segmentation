import torch
import datetime
import numpy as np
import torch.nn as nn

from utils.metrics import IoU
from torchsummary import summary
from utils.utils import create_dir
from utils.dataset import create_data
import settings as cfg
from utils.unet_transformer import SwinUnet
from utils.fit import fit, validation
from matplotlib import pyplot as plt
from torch.optim.lr_scheduler import StepLR
from ax.service.managed_loop import optimize
from utils.loss import DiceLoss, WeightedCrossEntropyDice
""" Configuration parameters """

bilinear = False
""" ************************ """
""" Directories """
create_dir("files")
create_dir("checkpoints")
checkpoint_path = "checkpoints/" + datetime.datetime.now().strftime("%d_%H-%M_SwinUnet_OCT/")
create_dir(checkpoint_path)

""" *********** """
""" CUDA device """
device = torch.device(f"cuda" if torch.cuda.is_available() else "cpu")
""" *********** """
exp = 0


def experiment(parameters):
    batch_size = parameters.get("batch_size", cfg.BATCH_SIZE)

    lr = parameters.get("lr", cfg.LEARNING_RATE)
    beta1 = parameters.get("beta1", cfg.BETA1)
    beta2 = parameters.get("beta2", cfg.BETA2)

    weight_decay = parameters.get("weight_decay", cfg.WEIGHT_DECAY)
    gamma_sch = parameters.get("gamma_sch", 0.8)
    num_epochs = parameters.get("num_epochs", cfg.EPOCHS)

    step_sch = parameters.get("step_sch", num_epochs*0.3)

    folder = checkpoint_path + datetime.datetime.now().strftime("%H-%M/") + "/"
    create_dir(folder)
    """ Dataset and loader """
    train_loader, val_loader = create_data()
    """ Building model """
    model = SwinUnet(in_chans=cfg.CHANNELS, img_size=cfg.IMAGE_SIZE, num_classes=cfg.CLASSES).to(device)
    model.state_dict()
    model.load_from("files/swin_tiny_patch4_window7_224.pth", device)
    model = model.to(device)
    summary(model, input_size=(3, 224, 224), batch_size=-1)
    if len(cfg.GPUS_ID) > 1:
        model = nn.DataParallel(model, device_ids=cfg.GPUS_ID)
    pytorch_total_params = sum(p.numel() for p in model.parameters())
    """ Prepare training """
    optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay=cfg.WEIGHT_DECAY, betas=(beta1, beta2))
    loss_fn = WeightedCrossEntropyDice(class_weights=[1, 1, 1], device=device)
    scheduler = StepLR(optimizer=optimizer, step_size=step_sch, gamma=gamma_sch)
    scaler = torch.cuda.amp.GradScaler()
    metrics = IoU(device)
    """ Save params """
    with open(folder + "LOGS.txt", "w") as text_file:
        text_file.write(f"Learning rate: {lr}\n")
        text_file.write(f"weight_decay: {weight_decay}\n")
        text_file.write(f"BETA1, BETA2: {cfg.BETA1, cfg.BETA2}\n")
        text_file.write(f"Optimizer: {cfg.OPTIMIZER}\n")
        text_file.write(f"Epochs: {num_epochs}\n")
        text_file.write(f"Loss function: {loss_fn.__name__}\n")
        text_file.write(f"Scheduler step, gamma: {step_sch, gamma_sch}\n")
        text_file.write(f"Batch size: {batch_size}\n")
        text_file.write(f"Factor LR, Factor M: {cfg.FACTOR_LR, cfg.FACTOR_M}\n")
        text_file.write(f"Metric: {metrics.__name__}\n")
        text_file.write(f"No. of Parameters: {pytorch_total_params}\n")
        text_file.write(f"No. of GPUs: {len(cfg.GPUS_ID)}\n")
        text_file.close()
    """ Training the model """
    fit(num_epochs=num_epochs,
        train_loader=train_loader,
        val_loader=val_loader,
        model=model,
        optimizer=optimizer,
        loss_fn=loss_fn,
        metric=metrics,
        scheduler=scheduler,
        scaler=scaler,
        device=device,
        checkpoint_path=folder,
        base_lr=lr,
        beta_1=beta1
        )
    """ Saving numpy history """
    hist_train_loss = np.load(folder + 'hist_train_loss.npy')
    hist_val_loss = np.load(folder + 'hist_val_loss.npy')
    hist_train_IoU = np.load(folder + 'hist_train_IoU.npy')
    hist_val_IoU = np.load(folder + 'hist_val_IoU.npy')

    hist_train_loss_iter = np.load(folder + 'hist_train_loss_iter.npy')
    hist_train_IoU_iter = np.load(folder + 'hist_train_IoU_iter.npy')

    x = np.arange(1, len(hist_train_loss) + 1)
    with open(folder + "results.txt", "w") as ff:
        ff.write(f"Best Train Loss: {np.min(hist_train_loss):0.4f}, Best Val Loss:{np.min(hist_val_loss):0.4f},\n "
                 f"Best Train IoU:{np.max(hist_train_IoU):0.4f}, Best Val IoU:{np.max(hist_val_IoU):0.4f}\n")
    plt.figure()
    plt.plot(x, hist_train_loss)
    plt.plot(x, hist_val_loss)
    plt.title("Loss")
    plt.grid(color='lightgray', linestyle='-', linewidth=2)
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validation'], loc='best')
    plt.savefig(folder + "loss.png")

    plt.figure()
    plt.plot(x, hist_train_IoU)
    plt.plot(x, hist_val_IoU)
    plt.title("IoU")
    plt.grid(color='lightgray', linestyle='-', linewidth=2)
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validation'], loc='best')
    plt.savefig(folder + "IoU.png")

    max_iterations = num_epochs * len(train_loader)

    plt.figure()
    plt.plot(range(max_iterations), hist_train_loss_iter)
    plt.grid(color='lightgray', linestyle='-', linewidth=2)
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.savefig(folder + "loss_iter.png")

    plt.figure()
    plt.plot(range(max_iterations), hist_train_IoU_iter)
    plt.grid(color='lightgray', linestyle='-', linewidth=2)
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.savefig(folder + "IoU_iter.png")

    load_best_model = torch.load(folder + 'model.pth')

    _, iou_eval = validation(model, val_loader, loss_fn, metrics, device, lr_=0.0, beta_1=0.0)
    return iou_eval



def main():
    best_parameters, values, exp, model = optimize(
        parameters=[
            {"name": "lr", "type": "range", "bounds": [1e-2, 9e-2], "log_scale": True},
            # {"name": "batch_size", "type": "range", "bounds": [64, 256]},
            # {"name": "weight_decay", "type": "range", "bounds": [0.0, 1.0]},
            {"name": "beta1", "type": "range", "bounds": [0.5, 0.9]},
            {"name": "beta2", "type": "range", "bounds": [0.5, 0.999]}
            # {"name": "num_epochs", "type": "range", "bounds": [300, 500]},
        ],
        total_trials=20,
        evaluation_function=experiment,
        objective_name='iou',
    )

    print(best_parameters)
    print(exp)
    means, covariances = values
    print(means)
    print(covariances)

if __name__ == '__main__':
    main()
