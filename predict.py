import os
import sys
import torch
import os

import torch
import h5py
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from utils.unet import Unet
import torch.nn as nn
from utils.metrics import IoU
import torch.nn.functional as F
from utils.dataset import OctDataset
import albumentations as T
from albumentations.pytorch import ToTensorV2
from torch.utils.data import DataLoader
from utils.fit import validation
import sys
from utils.loss import DiceLoss
from torchsummary import summary
from utils.unet_transformer import SwinUnet
import settings as cfg

def visualize(n, image, mask, pr_mask, path_save, name, metric_dict):
    """PLot images in one row."""
    figure, ax = plt.subplots(nrows=n, ncols=3)
    figure.set_figwidth(12)
    figure.set_figheight(10)
    for i in range(n):
        ax[i, 0].imshow(image[i, :, :], cmap='gray')
        ax[0, 0].title.set_text('Test image')
        ax[i, 0].axis('off')
        ax[i, 1].imshow(mask[i, :, :], cmap='jet')
        ax[0, 1].title.set_text('Test mask')
        ax[i, 1].axis('off')
        ax[i, 2].imshow(pr_mask[i, :, :], cmap='jet')
        ax[0, 2].title.set_text(f'Prediction \n{metric_dict[0]}')
        ax[i, 2].title.set_text(f'{metric_dict[i]}')
        ax[i, 2].axis('off')
    figure.suptitle(name)
    plt.savefig(path_save + "/" + name + '_' + str(np.random.randint(0, 100)) + ".png")
    plt.show()



def main():
    """ CUDA device """
    device = torch.device("cuda")
    PATH1 = 'checkpoints/25_08-30_UNET_OCT/model.pth',

    PATH2 = 'files/weights.pth',

    val_transforms = T.Compose(
        [
            # T.Resize(height=128, width=128),
            # T.Normalize(mean=[0.2991, 0.2991, 0.2991], std=[0.1757, 0.1757, 0.1757]),
            ToTensorV2(),
        ]
    )

    for path in PATH2:
        # load best saved checkpoint
        load = torch.load(path)
        channels = cfg.CHANNELS
        best_model = SwinUnet(in_chans=cfg.CHANNELS, img_size=cfg.IMAGE_SIZE, num_classes=cfg.CLASSES)
        best_model = best_model.to(device)
        # best_model = nn.DataParallel(best_model, device_ids=[0])
        best_model.load_state_dict(load)
        name = 'Swin-Unet'

        val_ds = OctDataset(image_dir=cfg.VAL_IMAGES_CROP,
                            mask_dir=cfg.VAL_MASKS_CROP,
                            transform=val_transforms,
                            channels=channels
                            )
        imgs = []
        mask_true = []
        prds_msk = []
        j = 3
        iou = IoU(device)
        # np.random.seed(20)  # 2, 10, 20, 42, 32
        res_metric = {}
        for i in range(j):
            randint = np.random.randint(low=0, high=len(val_ds))
            image, mask = val_ds[randint]

            image1 = image.unsqueeze(0).float().to(device)
            mask1 = mask.unsqueeze(0)
            mask1 = mask1.unsqueeze(0).long().to(device)

            pr_mask = best_model(image1)
            metric = iou(pr_mask, mask1)
            if cfg.CLASSES == 3:
                print_metric = f"BG:{metric[0]:2.3f}, OPL:{metric[1]:2.3f}, EZ:{metric[2]:2.3f}"
            elif cfg.CLASSES == 4:
                print_metric = f"BG:{metric[0]:2.3f}, OPL:{metric[1]:2.3f}, EZ:{metric[2]:2.3f}, ELM:{metric[3]:2.3f}"
            else:
                print("Invalid numb of class")
                sys.exit()
            res_metric[i] = print_metric
            print(print_metric)
            print(np.mean(metric))
            pr_mask = F.softmax(pr_mask, dim=1)
            pr_mask = torch.argmax(pr_mask, dim=1)
            pr_mask = (pr_mask.squeeze().cpu().float().detach().numpy())
            if channels == 3:
                image1 = np.transpose(image1.squeeze(0).squeeze(0).cpu().detach().numpy(), (1, 2, 0))
            else:
                image1 = image1.squeeze(0).squeeze(0).cpu().detach().numpy()
            mask1 = mask1.squeeze(0).squeeze(0).cpu().detach().numpy()
            print(image1.shape, mask1.shape)
            imgs.append(image1)
            mask_true.append(mask1)
            prds_msk.append(pr_mask)
        # print(np.array(imgs).shape, np.array(prds_msk).shape)
        # sys.exit()
        path_save = os.path.split(path)[0]
        visualize(n=len(imgs), image=np.array(imgs),
                  mask=np.array(mask_true), pr_mask=np.array(prds_msk),
                  path_save=path_save, name=name, metric_dict=res_metric
                  )


if __name__ == '__main__':
    main()
